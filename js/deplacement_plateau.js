export default function plateau() {

    var canvas = document.getElementById("myCanvas");
    var ctx = canvas.getContext("2d");
    var ballRadius = 20;
    var sachaX = ballRadius * 2;
    var sacha2X = canvas.height - ballRadius * 2;
    var sachaY = canvas.height - 2 * ballRadius;
    var sacha2Y = 2 * ballRadius;
    var rightPressed = false;
    var leftPressed = false;
    var upPressed = false;
    var downPressed = false;

    document.addEventListener("keydown", keyDownHandler, false);
    document.addEventListener("keyup", keyUpHandler, false);

    function keyDownHandler(e) {
        if (e.keyCode == 40) {
            downPressed = true;
        }
        else if (e.keyCode == 39) {
            rightPressed = true;
        }
        else if (e.keyCode == 38) {
            upPressed = true;
        }
        else if (e.keyCode == 37) {
            leftPressed = true;
        }
    }
    function keyUpHandler(e) {
        if (e.keyCode == 40) {
            downPressed = false;
        }
        else if (e.keyCode == 39) {
            rightPressed = false;
        }
        else if (e.keyCode == 38) {
            upPressed = false;
        }
        else if (e.keyCode == 37) {
            leftPressed = false;
        }
    }

    function drawSacha1() {
        ctx.beginPath();
        ctx.arc(sachaX, sachaY, ballRadius, 0, Math.PI * 2, false);
        ctx.fillStyle = "#0095DD";
        ctx.fill();
        ctx.closePath();
    }

    function drawSacha2() {
        ctx.beginPath();
        ctx.arc(sacha2X, sacha2Y, ballRadius, 0, Math.PI * 2, false);
        ctx.fillStyle = "#FF0000";
        ctx.fill();
        ctx.closePath();
    }

    function draw() {
        ctx.clearRect(0, 0, canvas.width, canvas.height);
        drawSacha1();
        drawSacha2();

        if (Math.sqrt(Math.pow(sachaX - sacha2X, 2) + Math.pow(sachaY - sacha2Y, 2)) < 2 * ballRadius) {
            alert("combat");
        }
        else if (upPressed && sachaY > ballRadius) {
            sachaY -= 3;
        }
        else if (leftPressed && sachaX > ballRadius) {
            sachaX -= 3;
        }
        else if (downPressed && sachaY < canvas.height - ballRadius) {
            sachaY += 3;
        }
        else if (rightPressed && sachaX < canvas.width - ballRadius) {
            sachaX += 3;
        }
    }

    setInterval(draw, 10);

}