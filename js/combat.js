// import { Perso } from "./perso.js";
// import { Pokemon } from "./pokemon.js";

export default class Combat {
    constructor(pokemon1, pokemon2) {
        this.pokemon1 = pokemon1;
        this.pokemon2 = pokemon2;
    }


    attack() {
        //tour 1 atk sacha1
        this.pokemon2.health -= this.pokemon1.hitStrength;
        alert(this.pokemon1.name + " de sacha1 attaque !!!");
        alert(this.pokemon2.name + " de sacha2 subit " + this.pokemon1.hitStrength + " dégats");
        if (this.Die(this.pokemon2)) {
            //vérification mort sacha2
            alert(this.pokemon2.name + " de sacha2 est mort");
            this.GameWin();
        } else {
            //tour 2 atk sacha2
            this.pokemon1.health -= this.pokemon2.hitStrength;
            alert(this.pokemon2.name + " de sacha2 attaque !!!");
            alert(this.pokemon1.name + " de sacha1 subit " + this.pokemon2.hitStrength + " dégats");
            if (this.Die(this.pokemon1)) {
                //vérification mort sacha1
                alert(this.pokemon1.name + " de sacha1 est mort");
                this.GameLoose();
            } else {
                //phase de retour tour 1
                this.attack();
            }
        }
    }

    Die(pokemon) {
        //vérification de la mort du perso
        return pokemon.health <= 0
    }

    GameWin() {
        alert("vous avez gagné");
        return;
    }

    GameLoose() {
        alert("vous avez perdu");
        return;
    }
}