export default class Pokemon {
    constructor(name, health, hitStrength, type) {
        this.name = name;
        this.health = health;
        this.hitStrength = hitStrength;
        this.type = type
    }

    getName() {
        return this.name;
    }

    getHealth() {
        return this.health;
    }
}